import 'dart:io';
import 'package:demo_app/academy.dart';
import 'package:demo_app/basic_data.dart';
import 'package:demo_app/national_identity.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';

enum SingingCharacter { lafayette, jefferson }

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  DateTime? pickedDate;
  String? gender;
  String? employer;
  File? _image;
  CroppedFile? _croppedFile;
  Future getImage(ImageSource source) async {
    final image = await ImagePicker().pickImage(
      source: source,
    );
    if (image == null) return;
    final saveImg = await saveImage(image.path);

    setState(() {
      this._image = saveImg;
    });
  }

  Future saveImage(String imagePath) async {
    final directory = await getApplicationDocumentsDirectory();
    final name = basename(imagePath);
    final image = File("${directory.path}/$name");
    return File(imagePath).copy(image.path);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF90A4AE),
          leading: Icon(Icons.arrow_back),
          title: const Text(
            "Update My Profile",
          ),
          bottom: const TabBar(
            indicatorColor: Colors.white,
            tabs: <Widget>[
              Tab(
                child: Text(
                  "Basic Data",
                ),
              ),
              Tab(
                child: FittedBox(
                    child: Text(
                  "National Identity",
                )),
              ),
              Tab(
                child: Text(
                  "Academy",
                ),
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            BasicData(),
            NationalIdentity(),
            Academy(),
          ],
        ),
     //    floatingActionButton: SizedBox(
     //      width: MediaQuery.of(context).size.width,
     //      child: FloatingActionButton.extended(
     //        isExtended: true,
     //
     //        onPressed: (){}, label: Text("Update"),
     // // child: Text("Update"),
     //      ),
     //    ),
      ),
    );
  }
}
