import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
class TestDropdown extends StatefulWidget {
  const TestDropdown({Key? key}) : super(key: key);

  @override
  State<TestDropdown> createState() => _TestDropdownState();
}

class _TestDropdownState extends State<TestDropdown> {
  String? selectedValue;
  List<String> items = [
    'Item11111111111111111111111111',
    'Item21111111111111111111111111',
    'Item31111111111111111111111111',
    'Item41111111111111111111111111',

  ];
  String taxvalue = 'Circle-140';
  var values = [
    'Circle-140',
    'Circle-141',
    'Circle-142',
    'Circle-143',
    'Circle-144',
    'Circle-145',
    'Circle-146',
    'Circle-147',
    'Circle-148',
    'Circle-149',
    "Circle-150",
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SizedBox(
       width: MediaQuery.of(context).size.width,
          child: DropdownButtonHideUnderline(
            child: DropdownButton2(
              hint: Text(
                'Select Item',
                style: TextStyle(
                  fontSize: 14,
                  color: Theme
                      .of(context)
                      .hintColor,
                ),
              ),
              items: items
                  .map((item) =>
                  DropdownMenuItem<String>(

                    value: item,
                    child: Text(
                      item,
                      style: const TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ))
                  .toList(),
              value: selectedValue,
              onChanged: (value) {
                setState(() {
                  selectedValue = value as String;
                });
              },
              buttonHeight: 40,
              buttonWidth: 140,
              itemHeight: 40,
          selectedItemHighlightColor: Colors.red,
            // itemWidth: 140,
            ),
          ),
        ),
      ),
    );
  }
}
