import 'dart:io';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
class BasicData extends StatefulWidget {
  const BasicData({Key? key}) : super(key: key);

  @override
  State<BasicData> createState() => _BasicDataState();
}

class _BasicDataState extends State<BasicData> {

  DateTime? pickedDate;
  String ?formattedDate;
  String? gender;
  String? employer;
  File? _image;
  String? ocupation;
  Future getImage(ImageSource source) async {
    final image = await ImagePicker().pickImage(
      source: source,
    );
    if (image == null) return;
    final saveImg = await saveImage(image.path);

    setState(() {
      this._image = saveImg;
    });
  }

  Future saveImage(String imagePath) async {
    final directory = await getApplicationDocumentsDirectory();
    final name = basename(imagePath);
    final image = File("${directory.path}/$name");
    return File(imagePath).copy(image.path);
  }

 pickDate(BuildContext context)async{
    pickedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1950),
        lastDate: DateTime.now());

    if (pickedDate != null) {
      print(
          pickedDate); //pickedDate output format => 2021-03-10 00:00:00.000
      formattedDate =DateFormat.yMMMd().format(pickedDate!);
      setState(() {

      });
    } else {}

  }
  @override
  Widget build(BuildContext context) {
    return  SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Stack(children: [
                CircleAvatar(
                  radius: 50,
                  backgroundColor: Color(0xFF90A4AE),
                  backgroundImage: _image != null
                      ? FileImage(
                    _image!,
                  )
                      : null,
                ),
                Positioned(
                    right: 0,
                    bottom: 10,
                    child: GestureDetector(
                      onTap: () {
                        getImage(ImageSource.camera);
                      },
                      child: Icon(Icons.camera_alt),
                    ))
              ]),
            ),
            TextFormField(
              decoration: InputDecoration(
                label: Text("Full Name"),
              ),
            ),
            TextFormField(
              decoration: InputDecoration(
                label: Text("Mobile Number"),
              ),
            ),
            TextFormField(
              decoration: InputDecoration(
                label: Text("Address"),
              ),
            ),
            Row(
              children: [
                Text(
                  "Gender : ",
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 16),
                ),
                SizedBox(
                  width: 120,
                  child: RadioListTile(
                    contentPadding: EdgeInsets.all(0),
                    title: Text("Male"),
                    value: "male",
                    groupValue: gender,
                    onChanged: (value) {
                      setState(() {
                        gender = value.toString();
                      });
                    },
                  ),
                ),
                SizedBox(
                  width: 150,
                  child: RadioListTile(
                    contentPadding: EdgeInsets.all(0),
                    title: Text("Female"),
                    value: "female",
                    groupValue: gender,
                    onChanged: (value) {
                      setState(() {
                        gender = value.toString();
                      });
                    },
                  ),
                ),
              ],
            ),
            TextFormField(
              decoration: InputDecoration(
                label: Text("Father Name"),
              ),
            ),
            TextFormField(
              decoration: InputDecoration(
                label: Text("Mother Name"),
              ),
            ),
            Row(
              children: [
                Text(
                  "Are you Employed? ",
                ),
                SizedBox(
                  width: 90,
                  child: RadioListTile(
                    contentPadding: EdgeInsets.all(0),
                    title: Text("Yes"),
                    value: "yes",
                    groupValue: employer,
                    onChanged: (value) {
                      setState(() {
                        employer = value.toString();
                      });
                    },
                  ),
                ),
                SizedBox(
                  width:90,
                  child: RadioListTile(
                    contentPadding: EdgeInsets.all(0),
                    title: Text("No"),
                    value: "no",
                    groupValue: employer,
                    onChanged: (value) {
                      setState(() {
                        employer = value.toString();
                      });
                    },
                  ),
                ),
              ],
            ),
            TextFormField(
              decoration: const InputDecoration(
                label: Text("Organization Name"),
              ),
            ),
            Row(
              children: [
                const Text(
                  "Occupation ",
                ),
                SizedBox(
                  width: 125,
                  child: RadioListTile(
                    contentPadding: EdgeInsets.all(0),
                    title: const Text("Business"),
                    value: "business",
                    groupValue: ocupation,
                    onChanged: (value) {
                      setState(() {
                        ocupation = value.toString();
                      });
                    },
                  ),
                ),
                SizedBox(
                  width:140,
                  child: RadioListTile(
                    contentPadding: EdgeInsets.all(0),
                    title: const Text("Job Holder"),
                    value: "job",
                    groupValue: ocupation,
                    onChanged: (value) {
                      setState(() {
                        ocupation = value.toString();
                      });
                    },
                  ),
                ),
              ],
            ),

            TextFormField(
              decoration: const InputDecoration(
                label: Text("Your Designation"),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
              Container(
              //  margin: EdgeInsets.symmetric(horizontal: 10),
                child:formattedDate!=null? Text("${formattedDate}"):Text("Choose Date of Birth"),

              ),
              ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                    primary: Color(0xFF90A4AE),
                  ),
                  onPressed:()=>pickDate(context), icon: Icon(Icons.calendar_month), label: Text("Birth Date"))
            ],)
          ],
        ),
      ),
    );
  }

}
